﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.Strategy
{
    public interface ISortStrategy
    {
        List<Car> SortedNewList(List<Car> cars);
        void SortedInLine(List<Car> cars);

    }
}
