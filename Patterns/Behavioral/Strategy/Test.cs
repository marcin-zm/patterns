﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.Strategy
{
    public class Test
    {

        public void Run()
        {
            var cars = InitData();
            Console.WriteLine("samochody nieposortowane");
            Print(cars);
            Console.WriteLine();
            Console.WriteLine("samochody posortowane po typie");
            var fullNameSort = new FullNameSort();
            fullNameSort.SortedInLine(cars);
            Print(cars);
            Console.WriteLine();
            Console.WriteLine("samochody posortowane po mocy");
            var hpSort = new HpSort();
            hpSort.SortedInLine(cars);
            Print(cars);


        }

        public void Print(List<Car> cars)
        {
            foreach (var item in cars)
            {
                Console.WriteLine(item.GetFullDescription());
            }
        }

        public List<Car> InitData()
        {
            var cars = new List<Car>()
            {
                new Car()
                {
                    Id = 1,
                    Brand = "VW",
                    Model = "Passat",
                    Engine = "1.9 TDI",
                    Doors = 4,
                    HP = 87
                },
                new Car()
                {
                    Id = 1,
                    Brand = "VW",
                    Model = "Golf IV",
                    Engine = "1.9 TDI",
                    Doors = 5,
                    HP = 95
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Renault",
                    Model = "Twingo",
                    Engine = "1.1",
                    Doors = 3,
                    HP = 55
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Mitsubishi",
                    Model = "Space Star",
                    Engine = "1.9 DID",
                    Doors = 5,
                    HP = 102
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Opel",
                    Model = "Astra IV",
                    Engine = "1.6",
                    Doors = 5,
                    HP = 116
                },
            };
            return cars;
        }

    }
}
