﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.Strategy
{
    public class HpSort : ISortStrategy
    {

        public void SortedInLine(List<Car> cars)
        {
            cars.Sort((x, y) => x.HP.CompareTo(y.HP));
        }

        public List<Car> SortedNewList(List<Car> cars)
        {
            return cars.OrderBy(x => x.GetName()).ToList();
        }
    }
}
