﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.ChainOfResponsibility.version1
{
    public class Test
    {
        public void Run()
        {

            Request requestOne = new ConcreteRequestOne();
            Request requestTwo = new ConcreteRequestTwo();

            Handler handlerOne = new ConcreteHandlerOne();
            Handler handlerTwo = new ConcreteHandlerTwo(handlerOne);

            handlerOne.Handle(requestOne);
            handlerOne.Handle(requestTwo);
        }

    }
}
