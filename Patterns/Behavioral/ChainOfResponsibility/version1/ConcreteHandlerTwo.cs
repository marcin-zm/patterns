﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.ChainOfResponsibility.version1
{
    class ConcreteHandlerTwo: Handler
    {
        public ConcreteHandlerTwo()
        {}

        public ConcreteHandlerTwo(Handler successor) : base(successor)
        {}

        protected override void OnHandle(Request request)
        {
            if (request.Category == Category.CategoryOne)
            {
                request.IsHandled = true;
            }
        }
    
    }
}
