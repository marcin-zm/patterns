﻿namespace Patterns.Behavioral.ChainOfResponsibility.version1
{
    public abstract class Request
    {
        public abstract Category Category { get;}
        public bool IsHandled { get; set; } 
    }
}
