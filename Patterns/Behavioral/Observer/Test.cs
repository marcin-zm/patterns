﻿using System;

namespace Patterns.Behavioral.Observer
{
    public class Test
    {
        public void Run()
        {
            var observable = new Observable();
            var observerOne = new ObserverOne();
            var observerTwo = new ObserverTwo();

            observable.Add(observerOne);
            observable.Notify("First notification");
            observable.Add(observerTwo);
            observable.Notify("Second notification");
            observable.Remove(observerOne);
            observable.Notify("Third notification");

        }
    }
}
