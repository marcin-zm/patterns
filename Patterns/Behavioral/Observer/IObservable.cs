﻿namespace Patterns.Behavioral.Observer
{
    public interface IObservable
    {
        void Add(IObserver observer);
        void Remove(IObserver observer);
    }
}
