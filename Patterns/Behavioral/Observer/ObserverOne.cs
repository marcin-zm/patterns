﻿using System;

namespace Patterns.Behavioral.Observer
{
    public class ObserverOne : IObserver
    {
        public void Message(string message)
        {
            Console.WriteLine("Message: {0}  received by: {1}", message,"ObserverOne");
        }
    }
}
