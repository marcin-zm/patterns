﻿using System.Collections.Generic;

namespace Patterns.Behavioral.Command
{
    public class Invoker
    {
        private List<Command> _commandList;
        private int _lastExecuted = -1;


        public Invoker()
        {
            _commandList = new List<Command>();
        }

        public void AddCommand(Command command)
        {
            _commandList.Add(command);
        }

        public void ClearCommands()
        {
            _commandList.Clear();
            _lastExecuted = -1;
        }

        public void ExecuteCommands()
        {
            for (int i = 0; i < _commandList.Count; i++)
            {
                _commandList[i].Execute();
                _lastExecuted = i;
            }
        }

        public void Rollback()
        {
            if (_lastExecuted<_commandList.Count && _lastExecuted >=0 )
	            {
	                for (int i = _lastExecuted; i > -1 ; i--)
	                {
	                    _commandList[i].UnExecute();
	                }
	                _lastExecuted = -1;
	            }
        }

        public void RollbackLast()
        {
            if (_lastExecuted < _commandList.Count && _lastExecuted >= 0)
            {
                _commandList[_lastExecuted].UnExecute();
                _lastExecuted--;
            }
        }

    }
}
