﻿namespace Patterns.Behavioral.Command
{
    public abstract class Command
    {
        protected Receiver _receiver;

        protected Command(Receiver receiver)
        {
            _receiver = receiver;
        }

        public abstract void Execute();
        public abstract void UnExecute();
    }
}
