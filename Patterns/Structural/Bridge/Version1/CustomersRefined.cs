﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Structural.Bridge.Version1
{
    public class CustomersRefined : CustomersBase
    {
        public CustomersRefined(string clientsGroup):base(clientsGroup)
        {  }

        public override void ShowAll()
        {
            Console.WriteLine("===========================");
            base.ShowAll();
            Console.WriteLine("===========================");
        }
    }
}
