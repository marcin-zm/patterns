﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Structural.Bridge.Version1
{
    public class CustomersBase
    {
        private Data<Customer> _data;
        protected string _clientsGroup;

        public CustomersBase(string clientsGroup)
        {
            this._clientsGroup = clientsGroup;
        }

        public Data<Customer> Data
        {
            set { _data = value; }
            get { return _data; }
        }

        public virtual void Next()
        {
            _data.NextRecord();
        }

        public virtual void Prior()
        {
            _data.PreviousRecord();
        }

        public virtual void Add(Customer customer)
        {
            _data.AddRecord(customer);
        }

        public virtual void Delete(Customer customer)
        {
            _data.DeleteRecord(customer);
        }

        public virtual void Show()
        {
            var customer = _data.GetCurrent();
            Console.WriteLine("Id: {0}, Name: {1} ", customer.Id, customer.Name);
        }

        public virtual void ShowAll()
        {
            Console.WriteLine("Customer Group: " + _clientsGroup);
            Console.WriteLine("-----------------------------------");
            var customers = _data.GetAll();
            foreach (var item in customers)
            {
                Console.WriteLine("Id: {0}, Name: {1} ", item.Id, item.Name);
            }

        }

    }
}
