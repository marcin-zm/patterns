﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Structural.Bridge.Version1
{
    public class CustomersData : Data<Customer>
    {
        private List<Customer> _customers;
        private int _current;

        public CustomersData()
        {
            _customers = new List<Customer>();
            _current = 0;
            Init();
            
        }

        private void Init()
        {
            _customers.Add(new Customer() { Id = 1, Name = "Name " + 1 });
            _customers.Add(new Customer() { Id = 2, Name = "Name " + 2 });
            _customers.Add(new Customer() { Id = 3, Name = "Name " + 3 });
            _customers.Add(new Customer() { Id = 4, Name = "Name " + 4 });
            _customers.Add(new Customer() { Id = 5, Name = "Name " + 5 });

        }

        public override void NextRecord()
        {
            if (_current < _customers.Count -1)
            {
                _current++;
            }
        }

        public override void PreviousRecord()
        {
            if (_current >0)
            {
                _current--;
            }
        }

        public override void AddRecord(Customer record)
        {
            _customers.Add(record);
        }

        public override void DeleteRecord(Customer record)
        {
            _customers.Remove(record);
        }

        public override Customer GetCurrent()
        {
            if(_customers.Count >0)
            {
                return _customers[_current];
            }
            return null;
        }

        public override IEnumerable<Customer> GetAll()
        {
            return _customers;
        }
    }
}
