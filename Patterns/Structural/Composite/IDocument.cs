﻿namespace Patterns.Structural.Composite
{
    public interface IDocument
    {
        void Process();
    }
}
