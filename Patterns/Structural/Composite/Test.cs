﻿using System;

namespace Patterns.Structural.Composite
{
    public class Test
    {
        public void Run()
        {
            var doc1 = new Document("DOK/1");
            var doc2 = new Document("DOK/2");
            Console.WriteLine("Dokument:");
            doc1.Process();

            var compositeDoc = new DocumentComposite();
            compositeDoc.Add(doc1);
            compositeDoc.Add(doc2);
            Console.WriteLine("Kompozytowy Dokument:");
            compositeDoc.Process();
        }

    }
}
