﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Structural.Decorator
{
    public class DocumentDecorator : IDocument
    {
        private readonly IDocument _document;
        public DocumentDecorator(IDocument document)
        {
            _document = document;
        }
        public void Process()
        {
            _document.Process();
            Console.WriteLine("DocumentDecorator processed");
        }
    }
}
