﻿using System;

namespace Patterns.Structural.Decorator.LazyDecorator
{
    public class DocumentDecorator : IDocument
    {
        private readonly Lazy<Document> _document;
        public DocumentDecorator(Lazy<Document> document)
        {
            Console.WriteLine("Decorator created");
            _document = document;
        }

        public void Process()
        {
            _document.Value.Process();
        }
    }
}
