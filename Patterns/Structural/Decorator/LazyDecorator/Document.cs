﻿using System;

namespace Patterns.Structural.Decorator.LazyDecorator
{
    public class Document : IDocument
    {
        public Document()
        {
            Console.WriteLine("Document Created");
        }
        public void Process()
        {
            Console.WriteLine("Base document processed");
        }
    }
}
