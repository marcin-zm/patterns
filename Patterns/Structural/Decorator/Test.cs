﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Structural.Decorator
{
    public class Test
    {
        public void Run()
        {
            var doc = new Document();
            Console.WriteLine("Dokument:");
            doc.Process();
            var decorDoc = new DocumentDecorator(doc); 
            Console.WriteLine("Dekorowany Dokument:");
            decorDoc.Process();
        }

    }
}
