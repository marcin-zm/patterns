﻿namespace Patterns.Structural.Decorator.PredicateDecorator
{
    public class PredicateStartsWith : IPredicate
    {
        public bool Test(IDocument document)
        {
            if (document.GetSignature().StartsWith("S"))
            {
                return true;
            }
            return false;
        }
    }
}
