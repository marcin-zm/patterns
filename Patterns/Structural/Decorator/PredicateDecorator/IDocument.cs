﻿namespace Patterns.Structural.Decorator.PredicateDecorator
{
    public interface IDocument
    {
        void Process();
        string GetSignature();
    }
}
