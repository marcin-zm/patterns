﻿using System;

namespace Patterns.Structural.Decorator.PredicateDecorator
{
    public class Test
    {
        public void Run()
        {
            var docA = new Document("AAA");
            var docS = new Document("SSS");
            IPredicate predicate = new PredicateStartsWith();
            var decorDoc1 = new DocumentDecorator(docA,predicate);
            var decorDoc2 = new DocumentDecorator(docS,predicate);
            Console.WriteLine("AAA and SSS added and processed:");
            decorDoc1.Process();
            decorDoc2.Process();
        }

    }
}
