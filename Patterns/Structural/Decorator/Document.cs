﻿using System;

namespace Patterns.Structural.Decorator
{
    public class Document : IDocument
    {
        public void Process()
        {
            Console.WriteLine("Base document processed");
        }
    }
}
