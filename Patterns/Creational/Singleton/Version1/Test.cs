﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Patterns.Creational.Singleton.Version1
{
    public class Test
    {
        public void Run()
        {

            var obj1 = Singleton.GetInstance();
            Console.WriteLine("First singleton value:{0}", obj1.Value);
            Console.WriteLine("wait a second");
            Thread.Sleep(1000);
            var obj2 = Singleton.GetInstance();
            Console.WriteLine("Second singleton value:{0}",obj2.Value);
        }


    }
}
