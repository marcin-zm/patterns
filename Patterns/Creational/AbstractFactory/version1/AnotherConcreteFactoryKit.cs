﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public class AnotherConcreteFactoryKit : AbstractFactoryKit

    {
        public override IProductOne CreateProductOne()
        {
            return new AnotherProductOne();
        }

        public override IProductTwo CreateProductTwo()
        {
            return new AnotherProductTwo();
        }
    }
}
