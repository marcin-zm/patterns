﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public interface IProductTwo
    {
        void Consume(IProductOne productOne);
    }
}
