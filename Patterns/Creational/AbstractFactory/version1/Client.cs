﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public class Client
    {
        IProductOne productOne;
        IProductTwo productTwo;

        public Client(AbstractFactoryKit abstractFactory)
        {
            productOne = abstractFactory.CreateProductOne();
            productTwo = abstractFactory.CreateProductTwo();
        }

        public void Run()
        {
            Console.WriteLine("Operacje klienta");
            productTwo.Consume(productOne);
        }

    }
}
