﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public class AnotherProductOne : IProductOne
    {
        public AnotherProductOne()
        {
            Console.WriteLine("AnotherProductOne created");
        }

    }
}
