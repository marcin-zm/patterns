﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public class ConcreteFactoryKitOne : AbstractFactoryKit
    {
        public override IProductOne CreateProductOne()
        {
            return new ProductOne();
        }

        public override IProductTwo CreateProductTwo()
        {
            return new ProductTwo();
        }
    }
}
