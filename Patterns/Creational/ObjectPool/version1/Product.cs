﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version1
{
    public class Product
    {
        private string _temporaryData;

        public Product(int id)
        {
            Id = id;
            Created = DateTime.Now;
            _temporaryData = "Empty and waiting";
        }

        public int Id { get; private set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        
        public void SetData(string data)
        {
            _temporaryData = data;
            Console.WriteLine("Data for product(Id={0}) set to: {1}",Id, data);
        }



    }
}
