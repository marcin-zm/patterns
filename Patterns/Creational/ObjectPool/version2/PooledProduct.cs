﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version2
{
    public class PooledProduct : IProduct
    {
            private Product internalProduct;
        private Pool<IProduct> pool;

        public PooledProduct(Pool<IProduct> pool)
    {
        if (pool == null)
            throw new ArgumentNullException("pool");

        this.pool = pool;
        this.internalProduct = new Product();
    }

    public void Dispose()
    {
        if (pool.IsDisposed)
        {
            internalProduct.Dispose();
        }
        else
        {
            pool.Release(this);
        }
    }

    public void Test()
    {
        internalProduct.Test();
    }
    }
}
