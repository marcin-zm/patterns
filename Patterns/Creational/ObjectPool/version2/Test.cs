﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version2
{
    public class Test
    {
        public void Run()
        {
            Pool<IProduct> pool = new Pool<IProduct>(5, p => new PooledProduct(p),  LoadingMode.Lazy, AccessMode.Circular);
            using (IProduct product = pool.Acquire())
            {
                product.Test();
            }
        }
    }
}
