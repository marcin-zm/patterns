﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.FactoryMethod.version1
{
    public class ConcreteCreatorTwo: Creator
    {
        public override IProduct Get()
        {
            return new ProductTwo();
        }
    }
}
