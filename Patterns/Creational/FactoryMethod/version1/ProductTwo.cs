﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.FactoryMethod.version1
{
    public class ProductTwo : IProduct
    {
        public ProductTwo()
        {
            Console.WriteLine("ProductTwo created");
        }
    }
}
